#!/usr/bin/env bash

set -ex

rm -rf arpack-ng/
rm -rf blaspp/
rm -rf chameleon-1.2.0/
rm -rf composyx-1.0.1/
rm -rf fabulous-1.1.3/
rm -rf lapackpp/
rm -rf morse_cmake/
rm -rf mumps-5.7.1.0/
rm -rf paddle/
rm -rf pastix-6.4.0/
rm -rf qr_mumps-3.1/
rm -rf scotch-v7.0.4/
rm -rf starpu-1.4.7/
