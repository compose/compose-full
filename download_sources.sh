#!/usr/bin/env bash

MORSECMAKE_GIT="c47785f30d214220e3f8bcf1591a762c02892c33"
BLASPP_GIT="v2023.11.05"
LAPACKPP_GIT="v2023.11.05"
ARPACK_GIT="3.9.1"
PADDLE_GIT="0.3.7"

set -ex

git clone https://gitlab.inria.fr/solverstack/morse_cmake.git
cd morse_cmake && git checkout $MORSECMAKE_GIT && cd ..

git clone https://github.com/icl-utk-edu/blaspp.git
cd blaspp && git checkout $BLASPP_GIT && cd ..

git clone https://github.com/icl-utk-edu/lapackpp.git
cd lapackpp && git checkout $LAPACKPP_GIT && cd ..

git clone https://github.com/opencollab/arpack-ng.git
cd arpack-ng && git checkout $ARPACK_GIT && cd ..

git clone --recursive https://gitlab.inria.fr/solverstack/paddle.git
cd paddle && git checkout $PADDLE_GIT && git submodule update && cd ..
cp -r morse_cmake/modules/* paddle/src/cmake_modules/morse/modules/

wget https://gitlab.inria.fr/api/v4/projects/52455/packages/generic/source/v1.0.1/composyx-1.0.1.tar.gz
tar xvf composyx-1.0.1.tar.gz
rm composyx-1.0.1.tar.gz
cp -r morse_cmake/modules/* ./composyx-1.0.1/cmake_modules/morse_cmake/modules/

wget https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.2.0/chameleon-1.2.0.tar.gz
tar xvf chameleon-1.2.0.tar.gz
rm chameleon-1.2.0.tar.gz
cp -r morse_cmake/modules/* ./chameleon-1.2.0/cmake_modules/morse_cmake/modules/

wget https://gitlab.inria.fr/api/v4/projects/2083/packages/generic/source/v1.1.3/fabulous-1.1.3.tar.gz
tar xvf fabulous-1.1.3.tar.gz
rm fabulous-1.1.3.tar.gz
cp -r morse_cmake/modules/* ./fabulous-1.1.3/cmake/morse_cmake/modules/

wget https://github.com/scivision/mumps/archive/refs/tags/v5.7.1.0.tar.gz
tar xvf v5.7.1.0.tar.gz
rm v5.7.1.0.tar.gz
# to download the real mumps tarball
cd mumps-5.7.1.0 && mkdir -p build && cd build && cmake .. && cd ..
rm build/ -rf && cd ..

wget https://files.inria.fr/pastix/releases/v6/pastix-6.4.0.tar.gz
tar xvf pastix-6.4.0.tar.gz
rm pastix-6.4.0.tar.gz
cp -r morse_cmake/modules/* ./pastix-6.4.0/cmake_modules/morse_cmake/modules/
cp -r morse_cmake/modules/* ./pastix-6.4.0/spm/cmake_modules/morse_cmake/modules/

wget https://gitlab.com/qr_mumps/qr_mumps/-/archive/3.1/qr_mumps-3.1.tar.gz
tar xvf qr_mumps-3.1.tar.gz
rm qr_mumps-3.1.tar.gz

wget https://gitlab.inria.fr/scotch/scotch/-/archive/v7.0.4/scotch-v7.0.4.tar.gz
tar xvf scotch-v7.0.4.tar.gz
rm scotch-v7.0.4.tar.gz

wget https://files.inria.fr/starpu/starpu-1.4.7/starpu-1.4.7.tar.gz
tar xvf starpu-1.4.7.tar.gz
rm starpu-1.4.7.tar.gz
