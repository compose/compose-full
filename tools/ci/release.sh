#!/usr/bin/env bash

set -x

RELEASENUM=`echo $CI_COMMIT_TAG | cut -d v -f 2`
mkdir -p compose-full-$RELEASENUM
cp CMakeLists.txt compose-full-$RELEASENUM
cp *.org compose-full-$RELEASENUM
cp *.patch compose-full-$RELEASENUM
cd compose-full-$RELEASENUM
../download_sources.sh
cd ..
tar czf compose-full-$RELEASENUM.tar.gz compose-full-$RELEASENUM/
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./compose-full-$RELEASENUM.tar.gz "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/source/$CI_COMMIT_TAG/compose-full-$RELEASENUM.tar.gz"

COMMAND=`echo curl --header \"Content-Type: application/json\" --header \"JOB-TOKEN: $CI_JOB_TOKEN\" \
  --data \'{ \"name\": \"$CI_COMMIT_TAG\", \
            \"tag_name\": \"$CI_COMMIT_TAG\", \
            \"ref\": \"$CI_COMMIT_REF_NAME\", \
            \"assets\": { \"links\": [{ \"name\": \"Download release compose-full-$RELEASENUM.tar.gz\", \"url\": \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/source/$CI_COMMIT_TAG/compose-full-$RELEASENUM.tar.gz\" }] } }\' \
  --request POST https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases`
eval $COMMAND
