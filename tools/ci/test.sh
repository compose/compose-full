#!/bin/bash

# exit on first error
set -e
# verbose
set -x

# configure
if [ "$OPTION" == "all" ]; then
    echo "all"
    cmake -B build -DEIGEN=ON -DCHAMELEON=ON -DFABULOUS=ON -DPADDLE=ON -DMUMPS=ON -DPASTIX=ON -DQRMUMPS=ON -DCMAKE_INSTALL_PREFIX=$PWD/build/install
elif [ "$OPTION" == "noweb" ]; then
    echo "noweb"
    ./download_sources.sh
    cmake -B build -DEIGEN=ON -DCHAMELEON=ON -DFABULOUS=ON -DPADDLE=ON -DMUMPS=ON -DPASTIX=ON -DQRMUMPS=ON -DCMAKE_INSTALL_PREFIX=$PWD/build/install -DNOWEB=ON
elif [ "$OPTION" == "mumpsint32" ]; then
    echo "mumps int32"
    cmake -B build -DMUMPS=ON -DPASTIX=OFF -DPADDLE=OFF -DCMAKE_INSTALL_PREFIX=$PWD/build/install
elif [ "$OPTION" == "pastixint64" ]; then
    echo "pastix int64"
    cmake -B build -DPASTIX=ON -DINT64=ON -DCMAKE_INSTALL_PREFIX=$PWD/build/install
else
    echo "default"
    cmake -B build -DCMAKE_INSTALL_PREFIX=$PWD/build/install
fi

# build and install
cmake --build build

# test
export LD_LIBRARY_PATH=$PWD/build/install/lib:$LD_LIBRARY_PATH
if [ "$OPTION" == "mumpsint32" ]; then
  mpiexec -n 8 --oversubscribe ./build/install/bin/composyx_driver_cg -P build/src/composyx-build/matrices/partitioned/ -N 8 -p AS --direct_solver Mumps
elif [ "$OPTION" == "pastixint64" ]; then
  mpiexec -n 8 --oversubscribe ./build/install/bin/composyx_driver_cg -P build/src/composyx-build/matrices/partitioned/ -N 8 -p AS --direct_solver Pastix
else
  mpiexec -n 8 --oversubscribe ./build/install/bin/composyx_driver_cg -P build/src/composyx-build/matrices/partitioned/ -N 8 -p AS
fi
