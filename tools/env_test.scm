;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.

(concatenate-manifests
  (list (specifications->manifest
          (list "coreutils"
                "binutils"
                "grep"
                "sed"
                "git"
                "cmake"
                "openssl"
                "make"
                "gcc-toolchain"
                "gfortran-toolchain"
                "nss-certs"
                "openblas"
                "curl"
                "wget"
                "tar"
                "bison"
                "flex"
                "patch"
                "perl"
                "python"
                "pkg-config"
                "openmpi"
                "hwloc"
                "gawk"))
        (package->development-manifest
          (specification->package "starpu"))))
