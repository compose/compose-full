FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get install -y vim git cmake build-essential emacs gfortran perl python-is-python3 \
    bison flex wget tar curl sed pkg-config libopenblas-dev liblapacke-dev libscalapack-openmpi-dev \
    libhwloc-dev libopenmpi-dev libmetis-dev libsuitesparse-dev libeigen3-dev \
    && rm -rf /var/lib/apt/lists/*

ARG USERNAME=gitlab
ARG USER_UID=1000
RUN groupadd -f -g $USER_UID $USERNAME && \
    useradd -u $USER_UID -g $USERNAME -ms /bin/bash $USERNAME && \
    mkdir -p /home/$USERNAME && \
    chown -R $USERNAME:$USERNAME /home/$USERNAME && \
    chmod g+s /home/$USERNAME
USER $USERNAME
SHELL ["/bin/bash", "-c"]
WORKDIR /home/$USERNAME
